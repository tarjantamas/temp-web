function display_graph(svg, links) {
    var nodes = {};
    links.forEach(function(link) {
        let sourceTokens = link.source.split("|");
        let sourceId = sourceTokens[0] + sourceTokens[1];

        link.source = nodes[sourceId] || (nodes[sourceId] = {
            name: link.source,
            title: sourceTokens[1].split('+'),
            selected: sourceTokens[2],
            id: 'a' + sourceId.replace(/[^0-9a-z]/gi, 't')
        });
        
        let targetTokens = link.target.split("|");
        let targetId = targetTokens[0] + targetTokens[1];

        link.target = nodes[targetId] || (nodes[targetId] = {
            name: link.target,
            title: targetTokens[1].split('+'),
            selected: targetTokens[2],
            id: 'a' + targetId.replace(/[^0-9a-z]/gi, 't')
        });
    });

    svg.append('svg:defs').selectAll('markers')
        .data(['end']).enter()
        .append('svg:marker')
        .attr('id', String)
        .attr('viewBox', '0 -5 10 10')
        .attr('refX', 11)
        .attr('refY', -0.35)
        .attr('markerWidth', 4)
        .attr('markerHeight', 4)
        .attr('orient', 'auto')
        .append('svg:path')
        .attr('d', 'M0,-5L10,0L0,5')
        .attr('class', 'arrow');

    let force = d3.layout.force()
        .nodes(d3.values(nodes))
        .links(links)
        .distance(100)
        .charge(-300)
        .start();

    let link = svg.selectAll('.link').data(links).enter().append('line').attr('class', 'link').attr('x1', function (d) {
        return d.source.x;
    }).attr('y1', function (d) {
        return d.source.y;
    }).attr('x2', function (d) {
        return d.target.x;
    }).attr('y2', function (d) {
        return d.target.y;
    }).attr('id', function (d) {
        return d.target.id + 'separator' + d.source.id
    });
    svg.selectAll('.link').data(links).each(function() {
        if (this.id.split('separator')[0] != this.id.split('separator')[1]) {
            d3.select(this).attr('marker-end', 'url(#end)');
        }
    });
    let node = svg.selectAll('.node').data(force.nodes()).enter().append('g').attr('class', 'node').attr('id', function (d) {
        return d.id
    }).call(force.drag);
    node.append('rect').attr('class', 'node').attr('x', 0).attr('y', 0).attr('width', 43).attr('height', function(d) {
        return (d.title.length * 2) + 3
    }).style('stroke', function(d) {
        if (d.selected === 'True') return 'red';
    });
    node.on('click', d => {
        toggleChildren(treeNodes[d.title[0].trim()]);
        render(data, treeNodes[d.title[0].trim()]);
    });
    node.on('mousedown', function(d) {
        d3.event.stopPropagation();
    });
    svg.selectAll('.node').each(function(d) {
        showAttr(d);
    });

    function showAttr(d) {
        for (var i = 0; i < d.title.length; i++) {
            let chain = svg.select('g#' + d.id).append('text')
                .attr('dx', 2)
                .attr('dy', String(i + 1) + 'em')
                .text(function(d) {
                return d.title[i]
            });
            if (i === 0) {
                chain.attr('class', 'titleSmall');
            } else {
                chain.attr('class', 'blackSmall');
            }
        }
    }
    force.on('tick', function() {
        link.attr('x1', function(d) {
            return d.source.x;
        }).attr('y1', function(d) {
            return d.source.y;
        }).attr('x2', function(d) {
            return d.target.x;
        }).attr('y2', function(d) {
            return d.target.y;
        });
        node.attr('transform', function(d) {
            return 'translate(' + d.x + ',' + d.y + ')';
        })
    });

    return force;
}