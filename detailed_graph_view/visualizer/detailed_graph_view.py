from core.models import Node
from core.services.DisplayDataService import DisplayDataService
import math
from pkg_resources import resource_string


class DetailedGraphView(DisplayDataService):
    def __init__(self):
        self.links = []
        self.found = []
        self.row_chars = 30
        self.graph = Node()

    def name(self):
        return "Detailed graph view"

    def identifier(self):
        return "detailed_graph_view"

    def format_node(self, node):
        node.node_id = node.node_id.replace('\\', '-')
        node.node_id = node.node_id.replace(";", "\;")
        node.node_id = node.node_id.replace(',', '\,')
        node.node_id = node.node_id.replace('"', '\\"')
        node.node_id = node.node_id.replace("'", "\\'")

        if node.name is None:
            return

        node.name = str(node.name).replace('"', '`')
        node.name = str(node.name).replace("'", "`")
        node.name = str(node.name).strip()
        node.name = " ".join(node.name.splitlines())

        if len(node.name) > self.row_chars and '+' not in node.name:
            parts = math.ceil(len(node.name) / self.row_chars)
            text = node.name
            node.name = ""
            for i in range(1, parts):
                node.name += text[(i - 1) * self.row_chars:i * self.row_chars] + '+'
            node.name += text[(parts - 1) * self.row_chars:]

    def format_attributes(self, node):
        attr = ""
        for a in node.attributes:
            if node.attributes[a] is None:
                continue

            curr_att = a + ":" + str(node.attributes[a])

            curr_att = curr_att.replace('\\', '-')
            curr_att = curr_att.replace(";", "\;")
            curr_att = curr_att.replace(',', '\,')
            curr_att = curr_att.replace('"', '`')
            curr_att = curr_att.replace("'", "`")
            curr_att = curr_att.strip()
            curr_att = " ".join(curr_att.splitlines())

            if len(curr_att) > self.row_chars:
                parts = math.ceil(len(curr_att) / self.row_chars)
                text = curr_att
                curr_att = ""
                for i in range(1, parts):
                    curr_att += text[(i - 1) * self.row_chars:i * self.row_chars] + '+'
                curr_att += text[(parts - 1) * self.row_chars:]
            attr += "+" + curr_att

        return attr

    def create_links(self, graph, node):
        self.format_node(node)
        node_attr = self.format_attributes(node)

        if node.node_id in graph.connections:
            for neighbour in graph.connections.get(node.node_id, []):
                self.format_node(neighbour)
                neighbour_attr = self.format_attributes(neighbour)

                connection = "{ source: '" + node.node_id + "|" + node.name + " " + node_attr + "|" + str(node.selected) + \
                             "', target:'" + neighbour.node_id + "|" + neighbour.name + " " + neighbour_attr + "|" \
                             + str(neighbour.selected) + "' },"

                if connection not in self.links:
                    self.links.append(connection)
        else:
            self.links.append(
                "{ source: '" + node.node_id + "|" + node.name + " " + node_attr + "|" + str(node.selected) +
                "', target:'" + node.node_id + "|" + node.name + " " + node_attr + "|"
                + str(node.selected) + "' },")

    def get_graph_links(self, graph):
        self.links = []
        if not graph:
            return "[]"

        links = "["

        for node in graph.nodes:
            self.create_links(graph, node)

        # for g in graph:
        #     self.create_links(g)
        for l in self.links:
            links += l
        links = links[:-1] + "];"
        return links

    def get_graph_script(self):
        return resource_string(__name__, 'detailed_graph.js').decode("utf-8")
