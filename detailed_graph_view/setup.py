from setuptools import setup, find_packages

setup(
    name="detailed-graph-view",
    version="0.1",
    packages=find_packages(),
    install_requires=['core>=0.1'],
    namespace_packages=['visualizer'],
    entry_points={
        'data.display':
            ['detailed_graph_view=visualizer.detailed_graph_view:DetailedGraphView'],
    },
    zip_safe=True,
)
