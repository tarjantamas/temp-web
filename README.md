## Data visualization
Design Patterns and Software Components class project - modular web application that allows core module to communicate with multiple input and output modules.  
Implemented input modules allow loading JSON files and output modules provide graph visualizations.

## Team members
* Nikola Zeljkovic - Detailed graph view
* Sara Sukovic - Temperature data reader
* Tamas Tarjan - Simple graph view
* Igor Tica - Instagram data reader

All team members worked on CORE module.