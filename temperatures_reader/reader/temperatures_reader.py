import json
import traceback
import urllib.request

from core_app.core.models import Node
from core_app.core.models import Graph
from core_app.core.services.LoadDataService import LoadDataService


class TemperaturesReader(LoadDataService):
    def __init__(self):
        self.id_map = {}
        self.node_map = {}

    def name(self):
        return "Temperatures reader"

    def identifier(self):
        return "temperatures_reader"

    def load(self, file_path="https://goo.gl/Qgv38p"):
        graph = self.create_graph(file_path)
        print(graph.toJSON())
        return graph.toJSON(), graph

    def add_neighbours(self, data, node):
        connect = []
        for i in data:
            temperature = i["item"]["condition"]["temp"]
            flag = False
            if abs(int(node.attributes["temperature"]) - int(temperature)) < 5:
                flag = True

            city = i["location"]["city"]
            if flag == True and (city != node.node_description):
                node_id = self.id_map.get(city, city)
                if node_id not in self.node_map:
                    self.node_map[node_id] = Node(node_id, city, False, {"temperature": i["item"]["condition"]["temp"]})

                new = self.node_map[node_id]

                connect.append(new)
        return connect

    def create_graph(self, file_path):
        try:
            with urllib.request.urlopen(file_path) as url:
                data = json.loads(url.read().decode())
        except Exception:
            traceback.print_exc()
            return [None]

        nodes = []
        connections = {}
        get_list = data["query"]["results"]["channel"]
        for item in get_list:
            city = item["location"]["city"]

            if city not in self.id_map:
                self.id_map[city] = self.id_map.get(city, city)

            node_id = self.id_map[city]

            if node_id not in self.node_map:
                self.node_map[node_id] = Node(node_id, city, False, {"temperature": item["item"]["condition"]["temp"]})

            node = self.node_map[node_id]

            connect = self.add_neighbours(get_list, node)
            connections[node.node_id] = connect
            nodes.append(node)

        return Graph(nodes, connections)
