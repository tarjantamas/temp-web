from setuptools import setup, find_packages

setup(
    name="temperatures-reader",
    version="0.1",
    packages=find_packages(),
    install_requires=['core>=0.1'],
    namespace_packages=['reader'],
    entry_points={
        'data.read':
            ['temperatures_reader=reader.temperatures_reader:TemperaturesReader'],
    },
    zip_safe=True,
)
