import json


class Node:
    def __init__(self, node_id=None, node_description="", selected=False, attributes={}):
        self.name = node_description
        self.node_id = node_id
        self.node_description = node_description
        self.selected = selected
        self.attributes = attributes
        self.children = [
            {"name": attrib + ": " + str(attributes[attrib]), "children": []} for attrib in attributes
        ]

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


class Graph:
    def __init__(self, nodes=[], connections={}):
        self.name = "root"
        self.children = [
            {"name": "nodes", "children": nodes}
        ]
        self.nodes = nodes
        self.connections = connections

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)
