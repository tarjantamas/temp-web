import pkg_resources
from django.apps import AppConfig


class CoreAppConfig(AppConfig):
    name = 'core_app.core'
    read_plugins = {}
    display_plugins = {}

    def ready(self):
        self.read_plugins = load_plugins("data.read")
        self.display_plugins = load_plugins("data.display")

    def get_display_plugins(self):
        display_plugins = []
        for plugin in self.display_plugins:
            display_plugins.append({
                "name": self.display_plugins[plugin].name(),
                "id": self.display_plugins[plugin].identifier()
            })

        if len(display_plugins) == 0:
            return ["No plugins installed"]

        return display_plugins

    def get_read_plugins(self):
        read_plugins = []
        for plugin in self.read_plugins:
            read_plugins.append({
                "name": self.read_plugins[plugin].name(),
                "id": self.read_plugins[plugin].identifier()
            })

        if len(read_plugins) == 0:
            return ['No plugins installed']

        return read_plugins


def load_plugins(ep_id):
    plugins = {}
    for ep in pkg_resources.iter_entry_points(group=ep_id):
        p = ep.load()
        print("{} {}".format(ep.name, p))
        plugin = p()
        plugins[ep.name] = plugin
    return plugins
