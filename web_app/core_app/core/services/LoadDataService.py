from abc import ABC, abstractmethod


class LoadDataService(ABC):
    @abstractmethod
    def name(self):
        pass

    @abstractmethod
    def identifier(self):
        pass

    @abstractmethod
    def create_graph(self, file_path):
        pass
