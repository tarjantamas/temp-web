from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^acquire_data', views.acquire_data, name="acquire_data"),
    url(r'^view', views.view, name="view"),
    url(r'^filter_data', views.filter_data, name="filter_data"),
    url(r'^search_data', views.search_data, name="search_data"),
    url(r'^reset', views.reset, name="reset")
]
