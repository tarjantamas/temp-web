from django.shortcuts import render
from django.apps import apps
import jsonpickle
from core.models import Graph

def _get_plugin_names():
    config = apps.get_app_config("core")
    return config.get_display_plugins(), config.get_read_plugins()


def _get_plugins():
    config = apps.get_app_config("core")
    return config.display_plugins, config.read_plugins


def index(request):
    display_plugins, read_plugins = _get_plugin_names()

    return render(request, 'index.html', {
        'viewPlugins': display_plugins,
        'readerPlugins': read_plugins
    })


def view(request):
    request.session["error"] = ""
    display_plugin_names, read_plugin_names = _get_plugin_names()
    display_plugins, read_plugins = _get_plugins()

    view_plugin_id = request.GET.get("view_plugin_id", None)
    if view_plugin_id is None:
        view_plugin_id = request.session["view_plug_id"]
    else:
        request.session["view_plug_id"] = view_plugin_id

    view_plugin = display_plugins.get(view_plugin_id, None)

    graph_json = request.session["j_graph"]
    graph = jsonpickle.decode(graph_json)

    if view_plugin is None:
        return render(request, 'plugin_not_found.html', {})

    graph_script = view_plugin.get_graph_script()

    mutable_graph_links = view_plugin.get_graph_links(graph)
    persisted_graph_links = view_plugin.get_graph_links(graph)

    return render(request, 'graph_view.html', {
        'viewPlugins': display_plugin_names,
        'readerPlugins': read_plugin_names,
        'data_loaded': graph is not None,
        'graph': graph_json,
        'mutable_graph_links': mutable_graph_links,
        'persisted_graph_links': persisted_graph_links,
        "graph_script": graph_script
    })


def acquire_data(request):
    request.session["error"] = ""
    display_plugin_names, read_plugin_names = _get_plugin_names()
    display_plugins, read_plugins = _get_plugins()
    request.session["search_param"] = ""
    read_plugin_id = request.GET.get("reader_plugin_id", None)

    read_plugin = read_plugins.get(read_plugin_id, None)

    if read_plugin is None:
        return render(request, 'plugin_not_found.html', {})
    else:
        request.session["reader_plugin_id"] = read_plugin_id

    graph_json, graph = read_plugin.load()
    request.session["j_graph"] = jsonpickle.encode(graph)
    request.session["initial_graph"] = jsonpickle.encode(graph)

    graph_json = request.session["j_graph"]
    return render(request, 'graph_view.html', {
        'viewPlugins': display_plugin_names,
        'readerPlugins': read_plugin_names,
        'data_loaded': graph is not None,
        'graph': graph_json
    })


def search_data(request):
    param = request.GET.get("search_param", None)
    request.session["search_param"] = param

    jgraph = request.session["j_graph"]
    graph = jsonpickle.decode(jgraph)

    for node in graph.nodes:
        search(param, node)

    jgraph = jsonpickle.encode(graph)
    request.session["j_graph"] = jgraph

    return view(request)


def search(param, node):
    node.selected = param.lower() in node.node_description.lower() or \
        param.lower() in node.name.lower()


def reset(request):
    initial = request.session["initial_graph"]
    request.session["j_graph"] = initial
    request.session["search_param"] = ""
    return view(request)


def filter_data(request):
    param = request.GET.get("filter_param", None)
    request.session["filter_param"] = param

    jgraph = request.session["initial_graph"]
    graph = jsonpickle.decode(jgraph)

    filtered_graph = filter_graph(graph, param)

    jgraph = jsonpickle.encode(filtered_graph)
    request.session["j_graph"] = jgraph

    return view(request)


def filter_connections(users_list, connections, nodes_list):
    return_list = []
    for user in users_list:
        if findUserInList(user.node_id, nodes_list):
            return_list.append(user)
    return return_list


def findUserInList(user_id, nodes_list):
    for node in nodes_list:
        if node.node_id == user_id:
            return True
    return False


def form_connections(nodes_list, connections):
    new_connections = {}
    for user_id in connections:
        if (not findUserInList(user_id, nodes_list)):
            continue
        one_connection = filter_connections(connections[user_id], connections, nodes_list)
        if len(one_connection) != 0:
            new_connections[user_id] = one_connection
    return new_connections

def filter_graph(graph, param):

    nodes_list = []
    for node in graph.nodes:
        if (param.lower() in node.node_description.lower() or param.lower() in node.name.lower()):
            nodes_list.append(node)

    new_connections = form_connections(nodes_list, graph.connections)
    return_graph = Graph(nodes_list, new_connections)
    return return_graph