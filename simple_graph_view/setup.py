from setuptools import setup, find_packages

setup(
    name="simple-graph-view",
    version="0.1",
    packages=find_packages(),
    install_requires=['core>=0.1'],
    namespace_packages=['visualizer'],
    entry_points={
        'data.display':
            ['simple_graph_view=visualizer.simple_graph_view:SimpleGraphView'],
    },
    zip_safe=True,
)
