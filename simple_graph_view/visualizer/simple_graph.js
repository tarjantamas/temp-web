function display_graph(svg, links){
    var nodes = {};
    links.forEach(function(link) {
        let sourceTokens = link.source.split('|');
        let sourceId = sourceTokens[0] + sourceTokens[1];

        link.source = nodes[sourceId] || (nodes[sourceId] = {
            name: link.source,
            title: sourceTokens[1],
            selected: sourceTokens[2],
            id: 'a' + sourceId.replace(/[^0-9a-z]/gi, 't')
        });

        let targetTokens = link.target.split('|');
        let targetId = targetTokens[0] + targetTokens[1];

        link.target = nodes[targetId] || (nodes[targetId] = {
            name: link.target,
            title: targetTokens[1],
            selected: targetTokens[2],
            id: 'a' + targetId.replace(/[^0-9a-z]/gi, 't')
        });
    });
    console.log(nodes);
    svg.append("svg:defs").selectAll("markers")
        .data(["end"]).enter()
        .append("svg:marker")
        .attr("id", String)
        .attr('viewBox', '0 -5 10 10')
        .attr('refX', 15)
        .attr('refY', -0.27)
        .attr('markerWidth', 6)
        .attr('markerHeight', 6)
        .attr('orient', 'auto')
        .append('svg:path')
        .attr('d', 'M0,-5L10,0L0,5')
        .attr('class', 'arrow');

    let force = d3.layout.force()
        .nodes(d3.values(nodes))
        .links(links)
        .distance(100)
        .charge(-300)
        .start()
        .on('end', function () {
            console.log(force.size());
        });

    let link = svg.selectAll('.link').data(links).enter().append('line').attr('class', 'link').attr("x1", function (d) {
        return d.source.x;
    }).attr("y1", function (d) {
        return d.source.y;
    }).attr("x2", function (d) {
        return d.target.x;
    }).attr("y2", function (d) {
        return d.target.y;
    }).attr('id', function (d) {
        return d.target.id + '@' + d.source.id
    });
    svg.selectAll('.link').data(links).each(function() {
        if (this.id.split('@')[0] !== this.id.split('@')[1]) {
            d3.select(this).attr("marker-end", "url(#end)");
        }
    });
    let node = svg.selectAll('.node').data(force.nodes()).enter().append('g').attr('class', 'node').call(force.drag);
    node.append('circle').attr("x", function(d) {
        return d.x;
    }).attr("y", function(d) {
        return d.y;
    }).attr("r", 5).style('stroke', function(d) {
        if (d.selected === 'True') return 'red';
    });

    node.on("click", function (d) {
        toggleChildren(treeNodes[d.title]);
        render(data, treeNodes[d.title]);
    });

    node.append("text").attr("dx", 8).attr("dy", ".35em").attr('class', 'black').text(function(d) {
        return d.title
    }).style('stroke', function(d) {
        if (d.selected === 'True') return 'red';
    }).style('fill', function(d) {
        if (d.selected === 'True') return 'red';
    });
    node.on('mousedown', function(d) {
        d3.event.stopPropagation();
    });
    force.on("tick", function() {
        link.attr("x1", function(d) {
            return d.source.x;
        }).attr("y1", function(d) {
            return d.source.y;
        }).attr("x2", function(d) {
            return d.target.x;
        }).attr("y2", function(d) {
            return d.target.y;
        });
        node.attr("transform", function(d) {
            return "translate(" + d.x + "," + d.y + ")";
        });
    });
    return force;
}