from core_app.core.services.DisplayDataService import DisplayDataService
from pkg_resources import resource_string


class SimpleGraphView(DisplayDataService):
    def __init__(self):
        self.links = []

    def name(self):
        return "Simple graph view"

    def identifier(self):
        return "simple_graph_view"

    def create_links(self, graph, node):
        if node.node_id in graph.connections:
            for neighbour in graph.connections.get(node.node_id, []):
                connection = "{ source: '" + node.node_id + "|" + node.name + "|" + str(node.selected) +\
                    "', target:'" + neighbour.node_id + "|" + neighbour.name + "|" + str(neighbour.selected) + "' },"
                if connection not in self.links:
                    self.links.append(connection)
        else:
            self.links.append(
                "{ source: '" + node.node_id + "|" + node.name + "|" + str(node.selected) +
                "', target:'" + node.node_id + "|" + node.name + "|" + str(node.selected) + "' },")

    def get_graph_links(self, graph):
        self.links = []
        if not graph:
            return "[]"

        links = "["

        for node in graph.nodes:
            self.create_links(graph, node)

        # for g in graph:
        #     self.create_links(g)
        for l in self.links:
            links += l
        links = links[:-1] + "];"
        return links

    def get_graph_script(self):
        return resource_string(__name__, 'simple_graph.js').decode("utf-8")
