import os
from core_app.core.models import Node
from core_app.core.models import Graph
from core_app.core.services.LoadDataService import LoadDataService
from pkg_resources import resource_string
import jsonpickle


class InstagramReader(LoadDataService):

    def name(self):
        return "Instagram data reader"

    def identifier(self):
        return "instagram_reader"

    def load(self, file_path="korisnik-1802580654.json"):
        return self.create_graph(file_path).toJSON(), self.create_graph(file_path)


    # NOTE : file name should have the following form:
    # ( optional keyword )-( user id ).json
    def create_graph(self, file_path):

        jsonfile = resource_string(__name__, file_path).decode("utf-8")
        # extracting user id from file name
        target_user = os.path.basename(file_path)

        # id of the user whose followings we analyze
        user_id = target_user.split("-")[1].split(".json")[0]

        # all nodes (users in this case)
        nodes = []
        connections_id = {}

        user_dictionary = {}
        node_dictionary = {}

        data = jsonpickle.decode(jsonfile)


        # iterating through all users
        for u_id in data:
            # we exclude user whose followings we are iterating through
            if u_id != user_id:
                connections_id[u_id] = []
            for user in data[u_id]:
                if user["pk"] != user_id:
                    user_dictionary[user["pk"]] = user
                if u_id != user_id and user["pk"] != user_id:
                    connections_id[u_id].append(user["pk"])

        for node_id in user_dictionary:
            # chosen attributes from user profile
            attributes = {
                "full_name": user_dictionary[node_id]["full_name"],
                "pk": user_dictionary[node_id]["pk"],
                "is_private": user_dictionary[node_id]["is_private"]
            }

            node_dictionary[node_id] = Node(node_id, user_dictionary[node_id]["username"], False, attributes)

            nodes.append(node_dictionary[node_id])

        connections = self.make_connections(node_dictionary, connections_id)

        return Graph(nodes, connections)

    def make_connections(self, node_dictionary, connections_id):
        connections = {}
        for id in connections_id:
            connections[id] = []
            for user_id in connections_id[id]:
                connections[id].append(node_dictionary[user_id])
        return connections
