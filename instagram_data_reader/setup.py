from setuptools import setup, find_packages

setup(
    name="insta-reader-plugin",
    version="0.1",
    packages=find_packages(),
    install_requires=['core>=0.1'],
    namespace_packages=['data_reader'],
    entry_points={
        'data.read':
            ['instagram_reader=data_reader.instagram_reader:InstagramReader'],
    },
    zip_safe=True
)